package io.classpath.kafka.examples.config;

public class AppConfig {
    public final static String applicationID = "CallbackProducer";
    public final static String bootstrapServers = "<broker-1>:9092,<broker-2>:9092,<broker-3>:9092";
    public final static String topicName = "callback-producer";
    public final static int numEvents = 100;
}